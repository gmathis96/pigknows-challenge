import React, {useEffect} from "react";
import {view} from '@risingstack/react-easy-state'
import ThemeProvider from "./Theme/ThemeProvider";
import MainLayout from "./Layouts/MainLayout";
import AppState from "./State/AppState";
import RandomUser from "./State/Requests/RandomUser";
import UserList from "./Components/UserList";
import UserModal from "./Components/Modals/UserModal";


function App() {
    useEffect(() => {
        AppState.init();
        RandomUser.init();
    }, [])

    return (
        <div className="App">
            <ThemeProvider>
                <MainLayout>
                    <UserList />
                    <UserModal />
                </MainLayout>
            </ThemeProvider>
        </div>
    );
}

export default view(App);
