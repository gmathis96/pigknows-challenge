import React from 'react';
import {view} from '@risingstack/react-easy-state';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import UserModalState from "../../State/UserModalState";
import RandomUser from "../../State/Requests/RandomUser";
import {Grid} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
        minWidth: '100%',
    },
});

function UserModal(){
    // Hooks need to be at the top of the components, they need to be ran unconditionally
    const classes = useStyles();

    // Prevent the modal from trying to render before any information is loaded
    if(RandomUser.results.length === 0) {
        return null;
    }


    const user = RandomUser.results[UserModalState.userIndex];
    const table = [{
        name: 'Title',
        value: user.name.title
    },{
        name: 'First Name',
        value: user.name.first
    },{
        name: 'Last Name',
        value: user.name.last
    },{
        name: 'Gender',
        // Capitalize the first char
        value: user.gender.charAt(0).toUpperCase() + user.gender.slice(1).toLowerCase()
    },{
        name: 'Phone Number',
        value: user.phone
    },{
        name: 'Cell Number',
        value: user.cell
    },{
        name: 'Street Number',
        value: user.location.street.number
    },{
        name: 'Street Name',
        value: user.location.street.name
    },{
        name: 'City',
        value: user.location.city
    },{
        name: 'State/Province',
        value: user.location.state
    },{
        name: 'Country',
        value: user.location.country
    },{
        name: 'Postal Code',
        value: user.location.postcode
    },{
        name: 'Timezone',
        value: `${user.location.timezone.description} (${user.location.timezone.offset})`
    },{
        name: 'Coordinates',
        value: `${user.location.coordinates.latitude}, ${user.location.coordinates.longitude}`
    }];

    return (
        <Dialog
            fullWidth={true}
            maxWidth={'sm'}
            open={UserModalState.isOpen}
            onClose={UserModalState.close}
            aria-labelledby="max-width-dialog-title"
        >
            <DialogTitle id="max-width-dialog-title">{`${user.name.title}. ${user.name.first} ${user.name.last}`}</DialogTitle>
            <DialogContent>
                <Grid container spacing={2}>
                    <Grid item sm={4}>
                        <img src={user.picture.large} style={{width: '100%'}} alt='Profile' />
                    </Grid>
                    <Grid item sm={8}>
                        <TableContainer component={Paper}>
                            <Table className={classes.table} size="small" aria-label="a dense table">
                                <TableBody>
                                    {table.map((data, index) => (
                                        <TableRow key={index}>
                                            <TableCell align="right" width='100px'>{data.name}</TableCell>
                                            <TableCell align="left">{data.value}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={UserModalState.close} color="primary">
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    );


}

export default view(UserModal);
