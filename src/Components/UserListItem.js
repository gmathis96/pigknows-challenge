import React from 'react';
import {view} from '@risingstack/react-easy-state'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import MoreIcon from '@material-ui/icons/MoreHoriz';
import UserModalState from "../State/UserModalState";

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 345,
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

function UserListItem(props) {
    const {user, userIndex} = props;
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardHeader
                avatar={
                    <Avatar aria-label="user" className={classes.avatar} src={user.picture.thumbnail} />
                }
                title={`${user.name.title}. ${user.name.first} ${user.name.last}`}
                subheader={user.location.country}
            />
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam libero nisi, efficitur sed ligula sed, rhoncus consectetur tellus.
                    Phasellus blandit, lectus ac rutrum scelerisque, lectus tellus dignissim mauris, at commodo dui lectus eu urna.
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <IconButton onClick={() => UserModalState.open(userIndex)}>
                    <MoreIcon />
                </IconButton>
            </CardActions>
        </Card>
    );
}

export default view(UserListItem)
