import {view} from '@risingstack/react-easy-state'
import UserListItem from './UserListItem'
import RandomUser from "../State/Requests/RandomUser";
import {Button, ButtonGroup, Grid, TextField, CircularProgress} from "@material-ui/core";

function RenderPagination(){
    const page = RandomUser.config.page;

    return (
        <ButtonGroup>
            <Button onClick={() => RandomUser.setPage(page - 1)}>{'<'}</Button>
            <Button onClick={() => RandomUser.setPage(1)}>{page}</Button>
            <Button onClick={() => RandomUser.setPage(page + 1)}>{'>'}</Button>
        </ButtonGroup>
    );
}

function UserList() {
    const {isLoading} = RandomUser;
    return(
        <>
            <Grid container spacing={0}>
                <Grid item xs={6}>
                    <TextField
                        id="standard-basic" label="Users Per Page"
                        value={RandomUser.config.results}
                        defaultValue={RandomUser.config.results}
                        onChange={(event) => {RandomUser.config.results = parseInt(event.target.value)}}
                        onBlur={RandomUser.doRequest}
                    />
                </Grid>
                <Grid xs={6}>
                    <Grid container direction="row" justify="flex-end">
                        <Grid item><Button onClick={RandomUser.newSeed}>New List</Button></Grid>
                    </Grid>
                </Grid>
            </Grid>
            {isLoading && <CircularProgress />}
            {!isLoading && <Grid
                container
                spacing={0}
                direction="row"
                alignItems="center"
                justify="space-evenly"
            >
                {RandomUser.results.map((user, index) => (
                    <Grid key={index} item xs={3} className='user-list-grid-item'>
                        <UserListItem user={user} userIndex={index} />
                    </Grid>
                ))}
            </Grid>}
            <Grid container direction="row" justify="flex-end">
                <Grid item>
                    {RenderPagination()}
                </Grid>
            </Grid>

        </>
    );
}

export default view(UserList);
