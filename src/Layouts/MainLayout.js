import React from 'react';
import {view} from '@risingstack/react-easy-state';
import {AppBar, Toolbar, Typography, Container, Paper} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import AppState from "../State/AppState";

import './MainLayout.css'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

function MainLayout(props){
    const classes = useStyles();

    return (
        <>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        {AppState.title}
                    </Typography>
                </Toolbar>
            </AppBar>
            <Container>
                <Paper className='MainLayoutContent'>
                    {props.children}
                </Paper>
            </Container>

        </>
    );
}

export default view(MainLayout);
