import {view} from '@risingstack/react-easy-state';
import {createMuiTheme, ThemeProvider} from "@material-ui/core";
import ThemeConfig from './ThemeConfig.json';
import './Theme.css';

const theme = createMuiTheme(ThemeConfig);


function AppThemeProvider(props){
    return (
        <ThemeProvider theme={theme}>
            {props.children}
        </ThemeProvider>
    );
}

export default view(AppThemeProvider);
