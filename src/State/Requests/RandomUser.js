import {store} from '@risingstack/react-easy-state';
import axios from "axios";
import {v4 as uuid} from "uuid";


/*
Defining the store as RandomUser because the `this` keyword is undefined within the object
 */
const RandomUser = store({

    // This is where our results data is stored, empty array by default
    results: [],

    // This is where our info is stored from the request, empty object by default.
    info: {},

    // Prevent app from trying to render empty data, set loading to true by default
    loading: true,

    // This is where we store our request config
    config: {
        results: 8,
        seed: uuid(),
        page: 1
    },

    // Any Kind of initialization code necessary to get the first request performed
    init: async () => {
        await RandomUser.doRequest();
    },

    newSeed: async () => {
        RandomUser.config.seed = uuid();
        RandomUser.config.page = 1;
        await RandomUser.doRequest();
    },

    setPage: async(page) => {
        if(page >= 1){
            RandomUser.config.page = page;
            await RandomUser.doRequest();
        }
    },

    // Actually perform the request.
    doRequest: async () => {
        RandomUser.loading = true;

        try{
            const {results, page, seed} = RandomUser.config;

            const {data} = await axios.get(`https://randomuser.me/api/?results=${results}&seed=${seed}&page=${page}`);

            RandomUser.results = data.results;
            RandomUser.info = data.info;

        }catch (e){
            console.error(e);
        }

        RandomUser.loading = false;
    }
});

export default RandomUser;
