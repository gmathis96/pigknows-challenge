import {store} from '@risingstack/react-easy-state';

const UserModalState = store({
    isOpen: false,
    userIndex: 0,

    open: (userIndex) => {
        UserModalState.userIndex = userIndex;
        UserModalState.isOpen = true;
    },

    close: () => {
        UserModalState.isOpen = false;
    }
});

export default UserModalState;
