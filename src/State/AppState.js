import {store} from '@risingstack/react-easy-state';


/*
Defining the store as AppState because the `this` keyword is undefined in some occasions, this makes it more stable
 */
const AppState = store({

    // Holds the title for the page
    title: '',

    // Any code needed to initialize the app state
    init(){
        AppState.setTitle();
    },

    // When Called Set the Document title and the Page Title
    setTitle(title = 'PigKnows Code Test'){
        AppState.title = title;
        document.title = title;
    }
});

export default AppState;
